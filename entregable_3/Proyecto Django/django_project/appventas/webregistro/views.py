from django.shortcuts import render
from .forms import RegistroForm
from django.shortcuts import render, redirect


# Create your views here.

def registro(request):
    form = RegistroForm()
    # context = {'form':form}
    # if request.method == 'POST':
    #     form = CreateUserForm(request.POST)
    #     if form.is_valid():
    #         form.save()
    #         return redirect('login')
    if request.POST:
        form = RegistroForm(request.POST)
        print(request.POST)
        print(form.is_valid())
        if form.is_valid():
            print("válido")
            form.save()
            return redirect('login')
        else:
            print("inválido")
            print(form.errors)
    return render(request, "register.html", {'form':RegistroForm})
