//Busca información en localstorage y hace un arreglo si está vacío
usuario = localStorage.getItem("usuarios");

if (usuario) {
  usuario = JSON.parse(usuario);
} else {
  usuario = [];
}

/*Valida si existe un usuario con la contraseña introducida. Si es así, lo lleva al inicio de sesión y este inicio se guarda
en el localstorage*/
document.getElementById('loginForm').addEventListener('submit', function (e) {
    e.preventDefault();
    var b;
    var em;;
    let username = document.getElementById('username').value;
    let password = document.getElementById('password').value;
  
    for (var a in usuario) {
      console.log(a);
      console.log(usuario[a]);
  
      console.log(usuario[a].username);
      if (username == usuario[a].username && password == usuario[a].contraseña) {
        b = true;
        em = usuario[a].email;
        nm = usuario[a].nombre;
      }
    }
    // Aquí puedes hacer una comprobación básica de nombre de usuario y contraseña.
    // Por simplicidad, vamos a asumir que el usuario "admin" con contraseña "1234" es válido.
    if (username === "admin" && password === "1234") {
      localStorage.setItem("isLoggedIn", "true"); // Simula que un usuario está logeado
      window.location.href = "store.html";
    } else if (b) {
      localStorage.setItem("isLoggedIn", "true");
      var info = {name: nm, user: username, email: em};
      localStorage.setItem("loggedUser", JSON.stringify(info));
      window.location.href = "store.html";
    } else {
      alert("Invalid credentials!");
    }
  });

//Lo siguiente es la validación básica de la documentación de Bootstrap, comprobando a través de HTML campos vacíos o que cumplan el requisito
  // Example starter JavaScript for disabling form submissions if there are invalid fields
(() => {
    'use strict'
  
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    const forms = document.querySelectorAll('.needs-validation')
  
    // Loop over them and prevent submission
    Array.from(forms).forEach(form => {
      form.addEventListener('submit', event => {
        if (!form.checkValidity()) {
          event.preventDefault()
          event.stopPropagation()
        }
  
        form.classList.add('was-validated')
      }, false)
    })
  })()