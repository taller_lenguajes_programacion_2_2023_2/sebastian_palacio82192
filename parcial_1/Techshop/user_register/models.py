from django.db import models

# Create your models here.
class Persona(models.Model):
    id = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=100)
    username = models.CharField(max_length=100) 
    class Meta:
        abstract = True
        
class Usuario(Persona):
    email = models.CharField(max_length=100, unique=True)
    password = models.CharField(max_length=100)
    fecha_registro = models.DateField()
    token = models.CharField(max_length=100,blank=True)
