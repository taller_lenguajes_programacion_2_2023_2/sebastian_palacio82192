from django.urls import path

from . import views

urlpatterns = [
    path("register/", views.register, name="register"),
    path("userLogin/", views.userLogin, name="userLogin"),
    path("store/",views.store,name="store")
] 