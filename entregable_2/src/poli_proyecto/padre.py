from conexion import Conexion
import pandas as pd


class Padre (Conexion):
    
    def __init__(
        self, id=0, nombre="", altura=0, ancho=0, tiempo_vida=0, estaExtinto=False 
    ):
        """Esta es la clase padre, la cual está basada en árbol

        Args:
            id (int, optional): _description_. Defaults to 0.
            nombre (str, optional): _description_. Defaults to "".
            altura (int, optional): _description_. Defaults to 0.
            ancho (int, optional): _description_. Defaults to 0.
            tiempo_vida (int, optional): _description_. Defaults to 0.
            estaExtinto (bool, optional): _description_. Defaults to False.

        Returns:
            _type_: _description_
        """

        self.__nombre = nombre
        self.__altura = altura
        self.__ancho = ancho
        self.__tiempo_vida = tiempo_vida
        self.__estaExtinto = estaExtinto
        self.__id = id

        @property
        def _id(self):
            return self.__id

        @_id.setter
        def _id(self, value):
            self.__id = value

        @property
        def _nombre(self):
            return self.__nombre

        @_nombre.setter
        def _nombre(self, value):
            self.__nombre = value

        @property
        def _altura(self):
            return self.__altura

        @_altura.setter
        def _altura(self, value):
            self.__altura = value

        @property
        def _ancho(self):
            return self.__ancho

        @_ancho.setter
        def _ancho(self, value):
            self.__ancho = value

        @property
        def _tiempo_vida(self):
            return self.__tiempo_vida

        @_tiempo_vida.setter
        def _tiempo_vida(self, value):
            self.__tiempo_vida = value

        @property
        def _estaExtinto(self):
            return self.__estaExtinto

        @_estaExtinto.setter
        def _estaExtinto(self, value):
            self.__estaExtinto = value

        super().__init__()
        
    """Funciones CRUD para clase padre"""       
        
    def inserDatos(self):
        datos = '{},"{}","{}","{}","{}","{}"'.format(self.__id,self.__nombre,self.__altura,self.__ancho,self.__tiempo_vida,self.__estaExtinto)
        self.insertar_datos(nom_tabla="padre",nom_columns="carga_arb",datos_carga=datos)


    def insertarExcel(self):
        ruta = "entregable_2/src/poli_proyecto/static/xlsx/Entidades.xlsx"
        self.df = pd.read_excel(ruta,sheet_name="Clase Padre")
        self.insert_Arbol()
    
        
    def create_Arbol(self):
        atributos = vars(self)
        if self.crear_tabla(nom_tabla="padre",datos_tbl="datos_arb"):
            print("Tabla Padre creada")
        
        return atributos
    
    def insert_Arbol(self):
        datos=""
        for index, row in self.df.iterrows():
            datos = '{},"{}","{}","{}","{}","{}"'.format(row["id"],row["nombre"],row["altura"],row["ancho"],row["tiempo_vida"],row["estaExtinto"])
            print(datos)
            self.insertar_datos(nom_tabla="padre",nom_columns="carga_arb",datos_carga=datos)
        return True
    
    def update_Arb(self,campoN,datoN,campoV,datoV):
        if(isinstance(datoN, int)):
            set = campoN+"="+str(datoN)
        else:
            set = campoN+'="'+datoN+'"'
        if(isinstance(datoV, int)):
            where = campoV+"="+str(datoV)
        else:
            where = campoV+'="'+datoV+'"'
        self.actualizar_tabla("padre",set,where)
        return True
    
    def delete_Arb(self,campo,dato):
        if(isinstance(dato, int)):
            where = campo+"="+str(dato)
        else:
            where = campo+'="'+dato+'"'
        self.borrar_tabla("padre",where)
        return True


