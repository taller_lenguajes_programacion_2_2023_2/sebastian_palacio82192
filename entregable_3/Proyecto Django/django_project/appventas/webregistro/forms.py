from django import forms
from django.forms import ModelForm

from weblogin.models import Usuario,Persona
from django.contrib.auth.forms  import UserCreationForm
from django.db import models


class RegistroForm(ModelForm):
    nombreCompleto = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-lg','placeholder':'Enter your full name','required':''}))
    email = forms.EmailField(widget=forms.TextInput(attrs={'class': 'form-control form-control-lg','placeholder':'Enter your full email address','type':'email','required':''}))
    f_nacimiento = forms.DateField(widget=forms.TextInput(attrs={'class': 'form-control form-control-lg','type':'date','required':''}))
    usuario = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-lg','placeholder':'Enter your username','required':''}))
    clave = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-lg','placeholder':'Enter your password','type':'password','required':''}))
    class Meta:
        model = Persona
        fields = ['usuario','clave','nombreCompleto','email','f_nacimiento']
        
        
# class CreateUserForm(ModelForm):
#     idrandom = randint(0,1000000)
#     print(idrandom)
#     nombre = 
#     username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-lg','placeholder':'Enter your username','required':''}))
#     email = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-lg','placeholder':'Enter your full email address','type':'email','required':''}))
#     password = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-lg','placeholder':'Enter your password','type':'password','required':''}))
#     fecha_registro = forms.CharField(widget=forms.TextInput(attrs={'type': 'hidden','value': date.today()}))
    
#     class Meta:
#         model: Usuario