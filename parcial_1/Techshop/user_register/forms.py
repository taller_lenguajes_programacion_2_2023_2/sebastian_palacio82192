from django.forms import ModelForm
# from django.contrib.auth.forms import UserCreationForm
# from django.contrib.auth.models import User
from django import forms

from .models import Usuario
# from .models import Persona
# from django.contrib.auth.forms import UserCreationForm
from datetime import date

from random import randint

# class UserForm(ModelForm):
#     class Meta:
#         model = Usuario
#         fields = '__all__'

class CreateUserForm(ModelForm):
    idrandom = randint(0,1000000)
    print(idrandom)
    id = forms.CharField(widget=forms.TextInput(attrs={'type': 'hidden','value': idrandom}))
    nombre = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-lg','placeholder':'Enter your full name','required':''}))
    username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-lg','placeholder':'Enter your username','required':''}))
    email = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-lg','placeholder':'Enter your full email address','type':'email','required':''}))
    password = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-lg','placeholder':'Enter your password','type':'password','required':''}))
    fecha_registro = forms.CharField(widget=forms.TextInput(attrs={'type': 'hidden','value': date.today()}))
   
    class Meta:
        model = Usuario
        fields = ['id','nombre','username','email','password','fecha_registro']
    idrandom = 0
    
