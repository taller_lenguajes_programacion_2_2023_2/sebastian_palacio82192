from django.shortcuts import render
from .forms import RegistroForm
from django.shortcuts import render, redirect
from django.contrib import messages


# Create your views here.

def registro(request):
    form = RegistroForm()
    # context = {'form':form}
    # if request.method == 'POST':
    #     form = CreateUserForm(request.POST)
    #     if form.is_valid():
    #         form.save()
    #         return redirect('login')
    if request.POST:
        form = RegistroForm(request.POST)
        print(request.POST)
        print(form.is_valid())
        if form.is_valid():
            print("válido")
            form.save()
        else:
            print("inválido")
            print(form.errors)
            messages.error(request, form.errors)
    return render(request, "register.html", {'form':RegistroForm})


def custom_404_view(request, exception=None):
    return render(request, '404.html', status=404)