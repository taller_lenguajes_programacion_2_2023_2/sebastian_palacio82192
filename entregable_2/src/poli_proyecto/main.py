from padre import Padre
from hijo import Hijo

def main():
    #IMPORTANTE: RECORDAR BORRAR EL ARCHIVO db_afcj_prueba tras cada ejecución, de lo contrario habrá problema al ejecutar script.
    
    """Funciones de crear tabla con datos para la clase padre
    """
    arbol = Padre(5,"roble",8,3)
    arbol.create_Arbol()
    arbol.insertarExcel()
    arbol.inserDatos()

    """Funciones de crear tabla con datos para la clase hijo
    """
    print("Acá va hijo")
    arbolito = Hijo("","","","","","","rojo","verde","azul",1)
    arbolito.insertarExcel()
    arbolito.inserDatos()
    print(arbolito)

    """Funciones de update para la clase padre
    """
    arbol.update_Arb("nombre","Pino",'id',1000)

    """Funciones de update para la clase hijo
    """
    arbolito.update_Hij("color_tronco","Morado",'tipo_corteza',"suave")
    
    """Funciones de borrado para padre e hijo
    """
    
    arbol.delete_Arb("id",1000)
    arbolito.delete_Hij("tipo_zona","tropical")
    
    """Funciones de lectura para ambas clases
    """
    arbol.leer_tabla("padre")
    arbolito.leer_tabla("hijo")


if __name__ == '__main__':
    main()