from django import forms
from .models import Usuario, Persona


class LoginForm(forms.Form):
    usuario = forms.CharField(required=True,widget=forms.TextInput(attrs={'class': 'form-control form-control-lg','placeholder':'Enter your username','required':''}
    ))
    clave =forms.CharField(required=True,widget=forms.PasswordInput(attrs={'class': 'form-control form-control-lg','placeholder':'Enter your password','type':'password','required':''}
        
    ))
    
    class Meta:
        model = Usuario

# class RegistroForm(forms.Form):
#     nombre = ""
#     apellido = ""
#     email = ""
#     f_nacimiento=""
#     # usuario = models.ForeignKey(Usuario,on_delete=models.SET_NULL,null=True)
    
#     class Meta:
#         model = Persona