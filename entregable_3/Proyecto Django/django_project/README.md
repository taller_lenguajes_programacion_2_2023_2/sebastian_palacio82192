# Proyecto en Django: Techshop



## Ejecución

Para ejecutar el proyecto, desde la ruta donde está este archivo se debe cambiar de directorio a appventas con cd .\appventas\ y luego ejecutar el comando python manage.py runserver. De esta manera, se debe dirigir a la dirección http://127.0.0.1:8000/ y esto llevará a la vista de inicio, la cual está vacía. Sin embargo, el servidor ya debe estar ejecutándose.

## Modelos de datos
Weblogin tiene las clases usuario y persona, en el cual usuario es abstracto y persona hereda de este modelo el usuario y la clave, los cuales se usan para el login.
Webventa tiene las clase productos, el cual se usa para mostrar los objetos en pantalla con su respectiva información e imágenes.


## Vistas existentes

```
/admin (por defecto de Django)
inicio
/login
/registro
/portal
/portal/detalle_producto/[id]
```

## admin

Esta vista muestra un login para superuser en el cual el Username es admin y la contraseña es 1234. Esto permite un acceso a las listas de modelos ya existentes con sus objetos ya definidos. Se puede leer, agregar, editar y borrar objetos de esta misma.

## inicio

Esta vista se accede solo con el localhost y el puerto 8000 (predeterminados por Django) pero no tiene ninguna ruta adicional a diferencia de todas las demás. Esta vista no tiene ninguna funcionalidad pero reemplaza la vista predeterminada de Django.

## Registro

Esta vista se usa para registrarse en la base de datos con los campos del modelo de persona. Cuando el registro es exitoso, redirige a la vista de login.

## login

Esta vista es el inicio de sesión para el usuario que se haya registrado en la base de datos y no es superuser. Se introduce el usuario y la contraseña.

## portal

Esta vista muestra el catálogo de productos completo, el cual se obtiene desde la base de datos y muestra los ítems existentes. Al hacer clic en la imagen de cualquier producto, muestra su página con detalle.

## portal/detalle_producto/id

Esta vista muestra el producto al cual se le ha hecho clic, desplegando su información registrada en la base de datos. El id en la dirección corresponde al id asignado en el modelo, el cual es secuencial.

## Organización

Cada app (webinicio, weblogin, webregistro y webventa) tiene su correspondiente carpeta de templates para el html. Sin embargo, todas consumen los recursos de imágenes, estilos y scripts que están en la carpeta static, dentro de la carpeta appventas del proyecto.