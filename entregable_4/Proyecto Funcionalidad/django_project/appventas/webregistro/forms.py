from django import forms
from django.forms import ModelForm

from weblogin.models import Usuario,Persona
from django.contrib.auth.forms  import UserCreationForm
from django.db import models



class RegistroForm(ModelForm):
    nombreCompleto = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-lg','placeholder':'Enter your full name','required':'','id':'nameInput','onkeyup':'check();'}))
    email = forms.EmailField(widget=forms.TextInput(attrs={'class': 'form-control form-control-lg','placeholder':'Enter your full email address','type':'email','required':'','id':'emailInput','onkeyup':'check();'}))
    f_nacimiento = forms.DateField(widget=forms.TextInput(attrs={'class': 'form-control form-control-lg','type':'date','required':'','id':'dob','onchange':'check();'}))
    usuario = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-lg','placeholder':'Enter your username','required':'', 'id':'username','onkeyup':'check();',"pattern":"^[a-zA-Z0-9]*$"}))
    clave = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-lg','placeholder':'Enter your password','type':'password','required':'','id':'pwd1','onkeyup':'check();',"pattern":".{6,}"}))
    class Meta:
        model = Persona
        fields = ['usuario','clave','nombreCompleto','email','f_nacimiento']
        