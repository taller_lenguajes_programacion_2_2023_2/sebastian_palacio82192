from django.db import models

# Create your models here.
class Usuario(models.Model):
    usuario = models.CharField(max_length=100)
    clave = models.CharField(max_length=100) 
    class Meta:
        abstract = True
        
class Persona(Usuario):
    nombreCompleto = models.CharField(max_length=100)
    email = models.CharField(max_length=100, unique=True)
    f_nacimiento = models.DateField()
