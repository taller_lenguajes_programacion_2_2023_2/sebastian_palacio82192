from conexion import Conexion
from padre import Padre
import pandas as pd


class Hijo (Padre,Conexion):
    def __init__(self, id=0, nombre="", altura=0, ancho=0, tiempo_vida=0, estaExtinto=False, color_tronco="", color_hojas="", tipo_zona="", ejemplares=0, tieneFrutas=False, tipo_corteza=""):
        super().__init__(nombre, altura, ancho, tiempo_vida, estaExtinto)
        
        
        self.__color_tronco =  color_tronco
        self.__color_hojas = color_hojas
        self.__tipo_zona = tipo_zona
        self.__ejemplares = ejemplares
        self.__tieneFrutas = tieneFrutas
        self.__tipo_corteza = tipo_corteza
        self.create_Hijo()
        """Clase hijo que hereda a padre

        Returns:
            _type_: _description_
        """
        """_summary_

        Returns:
            _type_: _description_
        """
        @property
        def _color_tronco(self):
            print("cotronco")
            return self.__color_tronco

        @_color_tronco.setter
        def _color_tronco(self, value):
            self.__color_tronco = value

        @property
        def _color_hojas(self):
            return self.__color_hojas

        @_color_hojas.setter
        def _color_hojas(self, value):
            self.__color_hojas = value

        @property
        def _tipo_zona(self):
            return self.__tipo_zona

        @_tipo_zona.setter
        def _tipo_zona(self, value):
            self.__tipo_zona = value

        @property
        def _ejemplares(self):
            return self.__ejemplares

        @_ejemplares.setter
        def _ejemplares(self, value):
            self.__ejemplares = value

        @property
        def _tieneFrutas(self):
            return self.__tieneFrutas

        @_tieneFrutas.setter
        def _tieneFrutas(self, value):
            self.__tieneFrutas = value

        @property
        def _tipo_corteza(self):
            return self.__tipo_corteza

        @_tipo_corteza.setter
        def _tipo_corteza(self, value):
            self.__tipo_corteza = value
         
    """Funciones CRUD para clase hijo"""   
    def inserDatos(self):
        datos = '"{}","{}","{}",{},"{}","{}"'.format(self.__color_tronco,self.__color_hojas,self.__tipo_zona,self.__ejemplares,self.__tieneFrutas,self.__tipo_corteza)
        print(datos + "datunos")
        self.insertar_datos(nom_tabla="hijo",nom_columns="carga_hij",datos_carga=datos)

    def create_Hijo(self,id=0):
        atributos = vars(self)
        if self.crear_tabla(nom_tabla="hijo",datos_tbl="datos_hij"):
            print("Tabla Hijo creada")
            
    def insertarExcel(self):
        ruta = "entregable_2/src/poli_proyecto/static/xlsx/Entidades.xlsx"
        self.df = pd.read_excel(ruta,sheet_name="Clase Hijo")
        self.insert_Hijo()
    
    def insert_Hijo(self):
        print("inserijo")
        datos=""
        for index, row in self.df.iterrows():
            datos = '"{}","{}","{}",{},"{}","{}"'.format(row["color_tronco"],row["color_hojas"],row["tipo_zona"],row["ejemplares"],row["tieneFrutas"],row["tipo_corteza"])
            print(datos)
            self.insertar_datos(nom_tabla="hijo",nom_columns="carga_hij",datos_carga=datos)
            return True
        
    def update_Hij(self,campoN,datoN,campoV,datoV):
        if(isinstance(datoN, int)):
            set = campoN+"="+str(datoN)
        else:
            set = campoN+'="'+datoN+'"'
        if(isinstance(datoV, int)):
            where = campoV+"="+str(datoV)
        else:
            where = campoV+'="'+datoV+'"'
        self.actualizar_tabla("hijo",set,where)
        return True
        
    def delete_Hij(self,campo,dato):
        if(isinstance(dato, int)):
            where = campo+"="+str(dato)
        else:
            where = campo+'="'+dato+'"'
        self.borrar_tabla("hijo",where)
        return True
        