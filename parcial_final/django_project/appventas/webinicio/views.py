from django.shortcuts import render,redirect
from django.http import HttpResponse

# Create your views here.

def inicio(request):
    mensaje={ 'txt_inicio':"Hola Mundo"
    }

    return render(request,'index.html',mensaje)

def custom_404(request, exception=None):
    return render(request, '404.html', status=404)