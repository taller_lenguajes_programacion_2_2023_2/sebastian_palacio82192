from django.db import models

Description = ""

class AuditoriaFecha(models.Model):
    f_creacion = models.DateTimeField(auto_now_add=True)
    f_actualizar = models.DateTimeField(auto_now=True)
    
    class Meta:
        abstract = True


class Productos(AuditoriaFecha):
    nom_producto = models.CharField(max_length=255)
    detalle_producto = models.CharField(max_length=255)
    estado = models.BooleanField(default=True)
    valor = models.FloatField()
    imagen = models.ImageField(default="Image path",upload_to='static/img/Productos')
    descripcion = models.TextField(max_length=255,default="Product description")
