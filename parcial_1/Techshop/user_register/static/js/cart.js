let cart = [];

// Si estás en la tienda, verifica si el usuario está logeado.
if (window.location.pathname.includes("store.html") && localStorage.getItem("isLoggedIn") !== "true") {
    window.location.href = "login.html";
}

//Toma los elementos del localstorage para el el log in
var logUs = localStorage.getItem("loggedUser");
logUs = JSON.parse(logUs);

//Muestra el nombre en HTML
greet = document.getElementById("greet");
greet.innerHTML = `<h3>Welcome to Techshop, ${logUs.name}.</h3>`;

//Arreglo donde se almacenan los productos
const productos = [

    {
        id: "1",
        titulo: "Nokia 1100",
        imagen: "../static/img/Images/1100.webp",
        content: "Lorem ipsum dolor",
        cost: 217.21,
    },
    {
        id: "2",
        titulo: "POCO F3",
        imagen: "../static/img/Images/F3.jPG",
        content: "Lorem ipsum dolor",
        cost: 123.211,
    },
    {
        id: "3",
        titulo: "Samsung S21 FE",
        imagen: "../static/img/Images/fe_grey1.jpg",
        content: "Lorem ipsum dolor",
        cost: 321.211,
    },
    {
        id: "4",
        titulo: "iPhone 13",
        imagen: "..//static/img/Images/iphone13.jfif",
        content: "Lorem ipsum dolor",
        cost: 234.211,
    },
    {
        id: "5",
        titulo: "iPhone 14",
        imagen: "../static/img/Images/iphone14.png",
        content: "Lorem ipsum dolor",
        cost: 432.211,
    },
    {
        id: "6",
        titulo: "Mi Watch",
        imagen: "../static/img/Images/mi watch.webp",
        content: "Lorem ipsum dolor",
        cost: 345.211,
    },
    {
        id: "7",
        titulo: "Earbuds",
        imagen: "../static/img/Images/s earbuds.jfif",
        content: "Lorem ipsum dolor",
        cost: 321.211,
    },
    {
        id: "8",
        titulo: "S23 Ultra",
        imagen: "../static/img/Images/s23.jfif",
        content: "Lorem ipsum dolor",
        cost: 765.211
    }
];

//Declaración de variables
const contenerProductos = document.querySelector("#contenedor-productos");
let btnsAddToCart = document.querySelectorAll(".agregar-producto");
const AtotalCartItems = document.querySelector("#cart-items");





const cartSection = document.querySelector("#carty");
const cartEmpty = document.querySelector("#empty-cart");

//Coloca los productos de manera dinámica en el HTML
function cargarProducto(productoselegidos) {
    productoselegidos.forEach(producto => {
        var div = document.createElement("div");
        div.classList.add("col-lg-3");
        div.classList.add("mb-4");
        div.classList.add("item-card");
        div.innerHTML = `
                    <div class="card h-100">
                        <img src="./Images/${producto.imagen}" class="card-img-top" alt="${producto.titulo}">
                        <div class="card-body">
                            <h5 class="card-title name">${producto.titulo}</h5>
                            <p class="card-text">${producto.content}</p>
                            <p class="card-text">$${producto.cost}</p>
                            <button type="button" class="btn btn-light add-cart agregar-producto" id="${producto.id}">Add to cart</button>

                        </div>
                    </div>
                `;

        contenerProductos.append(div);
    })
    updateAddBtns();

}

cargarProducto(productos);

//Inicia los botones de agregar al carrito
function updateAddBtns() {
    btnsAddToCart = document.querySelectorAll(".agregar-producto");

    btnsAddToCart.forEach(button => {
        button.addEventListener("click", addToCart);
    });
}

//Toma elementos del localstorage para el carro
let cartProducts;
let cartProductsLS = localStorage.getItem("items-in-cart");

//Valida si existe algo en el localstorage y si no, crea un arreglo para evitar errores
if (cartProductsLS) {

    cartProducts = JSON.parse(cartProductsLS);
    updateTotalCartItems();

} else {
    cartProducts = [];
}


let storedCartItems = localStorage.getItem("items-in-cart");
storedCartItems = JSON.parse(storedCartItems);

//Función para agregar al carrito
function addToCart(e) {
    const uid = e.currentTarget.id;
    console.log(uid);
    const addedProduct = productos.find(producto => producto.id === uid);

    if (cartProducts.some(producto => producto.id === uid)) {
        const index = cartProducts.findIndex(producto => producto.id === uid);
        cartProducts[index].cantidad++;
    } else {
        addedProduct.cantidad = 1;
        cartProducts.push(addedProduct);
    }

    updateTotalCartItems();
    storedCartItems = cartProducts;
    localStorage.setItem("items-in-cart", JSON.stringify(cartProducts));
    alert("Added to cart successfully ☺")
    loadCartProducts();
}

//Inicia la función para actualizar productos
function updateTotalCartItems() {
    let totalCartItems = cartProducts.reduce((i, producto) => i + producto.cantidad, 0);
    AtotalCartItems.innerText = totalCartItems;
}

const divisor = document.getElementById("divider");
let deleteButtons = document.querySelectorAll(".cart-item_delete");

const profInfo = document.querySelector("#profileInfo");
//Inserta información del usuario en dropdown
profInfo.innerHTML = `
<li><a class="dropdown-item " href="#">Name: ${logUs.user}.</a></li>
<li><a class="dropdown-item " href="#">Email: ${logUs.email}.</a></li>
<li><hr class="dropdown-divider"></li>
        <li><a class="dropdown-item" href="./login.html">Log out.</a></li>`;

//Carga los items del carro y los inserta en HTML
function loadCartProducts() {
    storedCartItems = localStorage.getItem("items-in-cart");
    storedCartItems = JSON.parse(storedCartItems);

    if (storedCartItems && storedCartItems.length > 0) {
        var cartFilled = document.createElement("div");

        cartEmpty.classList.add("disabled");
        cartFilled.classList.remove("disabled");
        console.log(storedCartItems + "lunotas");
        cartFilled.innerHTML = "";
        cartSection.innerHTML = "";
        storedCartItems.forEach(producto => {
            const div = document.createElement("div");
            div.classList.add("cart-items_stored");

            div.innerHTML = `
            <li><a class="dropdown-item " href="#">${producto.titulo} <img class="cart-item_delete" id=${producto.id} src="./img/Icons/8666597_trash_2_icon.svg" style="width: 20px; float: right;"></a></li>
            `;
            console.log(producto + "this");
            cartSection.append(div);
        });
        var divisor = document.createElement("li");
        divisor.innerHTML = `
        <li id="divider"><hr class="dropdown-divider"></li>
        <li ><a class="dropdown-item" id="total"></a></li>
        `;
        cartSection.append(divisor);
        console.log("carrot");

    } else {
        cartEmpty.classList.remove("disabled");
        cartSection.innerHTML = `<li id="empty-cart"><a class="dropdown-item">There are no items yet</a></li>`;
    }

    updateDelBtns();
    updateTotal();
}

loadCartProducts();

//Inicia función para borrar elementos del carro
function updateDelBtns() {
    deleteButtons = document.querySelectorAll(".cart-item_delete");

    deleteButtons.forEach(button => {
        button.addEventListener("click", deleteFromCart);

    });
}

//Busca el elemento en el array y lo borra, luego actualiza el array y el localstorage
function deleteFromCart(e) {
    storedCartItems = localStorage.getItem("items-in-cart");
    storedCartItems = JSON.parse(storedCartItems);

    const idbt = e.currentTarget.id;
    const index = storedCartItems.findIndex(producto => producto.id === idbt);
    storedCartItems.splice(index, 1);
    console.log(storedCartItems);
    localStorage.setItem("items-in-cart", JSON.stringify(storedCartItems));
    cartProducts = storedCartItems;
    loadCartProducts();
    console.log(storedCartItems)
    
}

//Muestra la sumatoria del total
function updateTotal() {
    const totalCalc = storedCartItems.reduce((i, producto) => i + (producto.cost * producto.cantidad), 0);
    total.innerText = "Total: $" + `${totalCalc}`;
}