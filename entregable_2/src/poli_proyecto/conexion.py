import sqlite3
import json
import re

class Conexion:
    def __init__(self) -> None:
        self.__user=""
        self.__password=""
        self.__puerto=0
        self.__url=""
        __nom_db="db_afcj_prueba.sqlite"
        self.__querys = self.obtener_json()
        self.__conex = sqlite3.connect(__nom_db)
        self.__cursor = self.__conex.cursor()
    
    def obtener_json(self):
        ruta = "entregable_2\src\poli_proyecto\static\sql\querys.json"
        querys={}
        with open(ruta, 'r') as file:
            querys = json.load(file) 
        return querys
    
    """Creación de BDS con JSON externo, adelante métodos CRUD
    """
    def crear_tabla(self, nom_tabla="",datos_tbl = ""):
        if nom_tabla != "" :
            datos=self.__querys[datos_tbl]
            query =  self.__querys["create_tb"].format(nom_tabla,datos)
            self.__cursor.execute(query)
            #print(query)
            return True
        else:
            return False
        
    def insertar_datos(self, nom_tabla="",nom_columns = "", datos_carga=""):
        if nom_tabla != "" :
            columnas=self.__querys[nom_columns]
            query =  self.__querys["insert"].format(nom_tabla,columnas,datos_carga)
            print(query+"cuerinsert")
            self.__cursor.execute(query)
            self.__conex.commit()
            return True
        else:
            return False
    
    def actualizar_tabla(self, nom_tabla,set,where):
        query =  self.__querys["update"].format(nom_tabla,set,where)
        self.__cursor.execute(query)
        self.__conex.commit()
        return True
    
    def borrar_tabla(self, nom_tabla,where):
        print(nom_tabla+"nomtabla")
        query =  self.__querys["delete"].format(nom_tabla,where)
        print(query+"cuero")
        self.__cursor.execute(query)
        self.__conex.commit()
        return True
    
    def leer_tabla(self,nom_tabla):
        query =  self.__querys["select"].format(nom_tabla)
        b = self.__cursor.execute(query).fetchall()
        print(b)
        return True

