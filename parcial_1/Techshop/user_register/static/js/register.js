usuario = localStorage.getItem("usuarios");/*Copia variable del localstorage,
que es donde se guarda la información*/

if (usuario) {
  usuario = JSON.parse(usuario);
} else {
  usuario = [];//Si está vacío el localstorage, hace un arreglo; de lo contrario daría error luego
}

var invalidCharsNum = [
  "1",
  "2",
  "3",
  "4", "5", "6", "7", "8", "9", "0"
];//Arreglo de números para referencia posterior

inputNumber();
function inputNumber() {
  inputBoxNum = document.querySelectorAll("#nameInput");

  inputBoxNum.forEach(button => {
    button.addEventListener("keydown", notNumber);
  });
}

function notNumber(e) {
  if (invalidCharsNum.includes(e.key)) {
    e.preventDefault();//Si la tecla escrita está dentro del arreglo evita que se escriba en el campo
  }
}

//Declaración de variables que surgen del documento HTML.
var feedbackEmail = document.getElementById("feedback-pwd");

var pwd1 = document.getElementById("pwd1");
var pwd2 = document.getElementById("pwd2");

var pwdModal = new bootstrap.Modal("#pwdModal");
var regModal = new bootstrap.Modal("#regModal");

var nombre = document.getElementById("nameInput");
var email = document.getElementById("emailInput");
var username = document.getElementById("username");

//Lo siguiente es la validación de formulario de Bootstrap con leves modificaciones.
(() => {

  'use strict'

  // Fetch all the forms we want to apply custom Bootstrap validation styles to
  const forms = document.querySelectorAll('.needs-validation')

  // Loop over them and prevent submission
  Array.from(forms).forEach(form => {

    form.addEventListener('submit', event => {
      if (!form.checkValidity()) {//Chequea si hay campos en blanco y si inputs son válidos
        if (pwd1.value != pwd2.value) {
          pwdModal.show();//Modal de Bootstrap si las contraseñas no coinciden
        }
        event.preventDefault()
        event.stopPropagation()
      } else { //Si es válido, mete un objeto "persona" en el arreglo y lo almacena en localstorage.
        const persona = { nombre: nombre.value, username: username.value, email: email.value, contraseña: pwd1.value };
        usuario.push(persona);
        console.log(usuario);
        localStorage.setItem("usuarios", JSON.stringify(usuario));
        regModal.show();//Exitoso
        event.preventDefault();
      }
      form.classList.add('was-validated')
    }, false)
  })
})()

var continuar = document.getElementById("continuar");

continuar.addEventListener("click", siguiente);

function siguiente(e) {
  window.location.href = "login.html";
}
