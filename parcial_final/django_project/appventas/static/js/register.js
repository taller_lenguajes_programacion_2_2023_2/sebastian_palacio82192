//Declaración de variables que surgen del documento HTML.
var error = false
var feedbackEmail = document.getElementById("feedback-pwd");

var pwd1 = document.getElementById("pwd1");
var pwd2 = document.getElementById("pwd2");

var pwdModal = new bootstrap.Modal("#pwdModal");
var regModal = new bootstrap.Modal("#regModal");
var emptyFieldModal = new bootstrap.Modal("#emptyFieldModal");


var nombre = document.getElementById("nameInput");
var email = document.getElementById("emailInput");
var username = document.getElementById("username");
var dob = document.getElementById("dob");


var invalido = document.getElementById("invalido");
var invalid = document.getElementById("invalida");

var invalidCharsNum = [
  "1",
  "2",
  "3",
  "4", "5", "6", "7", "8", "9", "0"
];//Arreglo de números para referencia posterior

inputNumber();
function inputNumber() {
  inputBoxNum = document.querySelectorAll("#nameInput");

  inputBoxNum.forEach(button => {
    button.addEventListener("keydown", notNumber);
  });
}

function notNumber(e) {
  if (invalidCharsNum.includes(e.key)) {
    e.preventDefault();//Si la tecla escrita está dentro del arreglo evita que se escriba en el campo
  }
}

function pwdNotMatching(e) {
  var valorActualizado1 = getValor1()
  var valorActualizado2 = getValor2()
  console.log(valorActualizado1 + " Valor 1")
  console.log(valorActualizado2 + " Valor 2")
  console.log(e.key)
  if (pwd2.value != pwd1.value) {
    console.log("desigual a")
    console.log(pwd1.value + " pwd1")
    console.log(pwd2.value + " pwd2")
    pwd2.classList.remove("is-valid")
    pwd2.classList.remove("was-validated")
    pwd2.classList.add("is-invalid")
  }
  else {
    console.log("igual a")
  }
}


function getValor1() {
  return pwd1.value
}

function getValor2() {
  return pwd2.value
}

//Lo siguiente es la validación de formulario de Bootstrap con leves modificaciones.
(() => {

  'use strict'

  // Fetch all the forms we want to apply custom Bootstrap validation styles to
  const forms = document.querySelectorAll('.needs-validation')
  var valido = true;
  // Loop over them and prevent submission
  Array.from(forms).forEach(form => {

    form.addEventListener('submit', event => {
      var bandera = false;
      error = true
      if (!nombre.checkValidity()) {
        nombre.classList.add("is-invalid")
        bandera = true;
        console.log("bandera")
      } else {
        nombre.classList.add("is-valid")
      }

      if (!username.checkValidity()) {
        username.classList.add("is-invalid")
        bandera = true;
        console.log("bandera")
      } else {
        username.classList.add("is-valid")
      }

      if (!dob.checkValidity()) {
        dob.classList.add("is-invalid")
        bandera = true;
        console.log("bandera")
      } else {
        dob.classList.add("is-valid")
      }

      if (!email.checkValidity()) {
        email.classList.add("is-invalid")
        bandera = true;
        console.log("bandera")
      } else {
        email.classList.add("is-valid")
      }

      if (!pwd1.checkValidity()) {
        pwd1.classList.add("is-invalid")
        bandera = true;
        console.log("bandera")
      } else {
        pwd1.classList.add("is-valid")
      }

      // if (!pwd2.checkValidity()) {
      //   pwd2.classList.add("is-invalid")
      //   bandera = true;
      // } else {
      //   pwd2.classList.add("is-valid")
      // }
      console.log("bandera " + bandera)
      if (bandera == true) {
        event.preventDefault()
        event.stopPropagation()
        emptyFieldModal.show()
      }
      else if (pwd1.value != pwd2.value) {
        console.log(getValor1())
        console.log(getValor2())
        pwdModal.show();//Modal de Bootstrap si las contraseñas no coinciden
        event.preventDefault()
        event.stopPropagation()
        valido = false;
      }
      else {
        regModal.show();//Exitoso
      }
    }, false)
  })
  return false;
})()

var check = function () {
  if (error) {
    if (pwd1.value != pwd2.value) {
      pwd2.classList.remove("is-valid")
      pwd2.classList.add("is-invalid")
    } else {
      pwd2.classList.remove("is-invalid")
      pwd2.classList.add("is-valid")
      console.log("coinciden")
    }

    if (pwd1.value == "" || pwd1.value.length < 6) {
      pwd1.classList.remove("is-valid")
      pwd1.classList.add("is-invalid")
    } else {
      pwd1.classList.remove("is-invalid")
      pwd1.classList.add("is-valid")
    }

    if (pwd2.value == "") {
      pwd2.classList.remove("is-valid")
      pwd2.classList.add("is-invalid")
    }

    if (!nombre.checkValidity()) {
      nombre.classList.remove("is-valid")
      nombre.classList.add("is-invalid")
    } else {
      nombre.classList.remove("is-invalid")
      nombre.classList.add("is-valid")
    }

    if (!dob.checkValidity()) {
      dob.classList.remove("is-valid")
      dob.classList.add("is-invalid")
    } else {
      dob.classList.remove("is-invalid")
      dob.classList.add("is-valid")
    }

    if (!username.checkValidity()) {
      username.classList.remove("is-valid")
      username.classList.add("is-invalid")
    } else {
      username.classList.remove("is-invalid")
      username.classList.add("is-valid")
    }

    if (!email.checkValidity()) {
      email.classList.remove("is-valid")
      email.classList.add("is-invalid")
    } else {
      email.classList.remove("is-invalid")
      email.classList.add("is-valid")
    }
  }
}